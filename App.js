import React from 'react';
import Navigation from './src/Navigation';
import NavigationService from './src/Navigation/NavigationService';
import AsyncStorage from '@react-native-community/async-storage';
import Languages from '@Config/Languages';
import Constants from '@Config/Constants';
import { Root } from "native-base";
import { Provider } from 'react-redux';
import Store from './src/Redux/Store';
console.disableYellowBox=true

class App extends React.Component{
  UNSAFE_componentWillMount=async()=>{
   let lang=await AsyncStorage.getItem(Constants.Language)
   console.log(lang)
     Languages.setLanguage(lang ? lang : "en")
  }
  render(){
  return (
    <Provider store={Store}>
    <Root>
    <Navigation
    ref={navigatorRef => {
      NavigationService.setTopLevelNavigator(navigatorRef);
    }}
    />
    </Root>
    </Provider>
  )
  }
};

export default App;
