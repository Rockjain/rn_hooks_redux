import React from 'react';
import { View,Text,StyleSheet,Image } from 'react-native';
import { Icon } from 'native-base';
import Images from '@Config/Images';
import Constants from '@Config/Constants';
import Language from '@Config/Languages';
const Feedback=({navigation})=>{

    return(
        <View style={styles.container}>
         <View style={styles.headerFeedback}>
             <Text style={{fontFamily:Constants.fontFamily.bold}}>{Language.feedback}</Text>
             <Icon name='ios-arrow-forward' style={{fontSize:18}} onPress={()=>navigation.navigate('fbList')}/>
         </View>
         <View style={styles.feedbackProfile}>
             <View style={styles.feedbackProfile}>
          <Image source={Images.Group} style={styles.bellIcon}/>
          <Text style={{fontFamily:Constants.fontFamily.semibold}}>{Language.teacher}</Text>
          </View>
          <View style={styles.feedbackProfile}>
          <Text style={{fontFamily:Constants.fontFamily.light}}>17:00</Text>
          <Text style={{marginLeft:5,fontFamily:Constants.fontFamily.light}}>{Language.june}</Text>
          </View>
         </View>
              <Text style={{marginTop:10,fontFamily:Constants.fontFamily.light}}>
              {Language.great}
              {Language.ddGreat} 
              {Language.great} 
              {Language.great}
              {Language.ddGreat} 
              {Language.great}
               </Text>
        </View>
    )
}


const styles=StyleSheet.create({
    container:{
        shadowColor:'black',
        shadowOpacity:0.25,
        shadowRadius:8,
        shadowOffset:{height:2,width:0},
        elevation:5,
        borderRadius:10,
        backgroundColor:'white',
        height:176,
        margin:20,
        padding:20
    },
    headerFeedback:{
        flexDirection:'row',
        justifyContent:'space-between',
    },
    feedbackProfile:{
        flexDirection:'row',
        justifyContent:'space-between',
        alignItems:'center'
       
    },
    bellIcon:{
        height:38,
        width:38,
       
    }
})
export default Feedback;