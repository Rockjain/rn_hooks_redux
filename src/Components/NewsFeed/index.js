import React from 'react';
import {View,Text,StyleSheet,Image,Dimensions,FlatList,TextInput} from 'react-native';
import Colors from '@Config/Colors';
import Constants from '@Config/Constants';
import Images from '@Config/Images';
const {width,height}=Dimensions.get('window')

const NewsFeed = ({Feed}) => {
  if (!Feed) {
    return null;

  }
  return(
     
     <View style={styles.container}>
         <View style={styles.userView}>
           <Image source={Feed.avatar} style={styles.iconStyle}/> 
           <Text style={styles.nameStyle}>{Feed.name}</Text>
         </View>
         <Image source={{uri: 'data:image/jpeg;base64,' +Feed.feed_Image}} style={styles.imageStyle}/> 
         <Text style={styles.tagStyle}>{Feed.feed_Title}</Text>

         <View style={[styles.likeView,{marginBottom:10}]}>
            <View style={styles.likeView}>
                <View style={styles.likeView}>
                    <View style={styles.likeView}>
                    <Image source={Images.Likes} style={styles.likesIcon}/>  
                    <Text style={{marginLeft:5}}>{Feed.likes}</Text>
                    </View>
                </View>
                <View style={styles.likeView}>
                    <View style={styles.likeView}>
                    <Image source={Images.Comments} style={styles.likesIcon}/>  
                    {/* <Text style={{marginLeft:5}}>{Feed.comments.length}</Text> */}
                    </View>
                </View>
            </View>

            <View style={styles.likeView}>
              
                <Image source={Images.Tag} style={styles.tagIcon}/>
                <Image source={Images.Comment} style={styles.likeIcon}/>
                <Image source={Images.Like} style={styles.likeIcon}/>  
                   
            </View>

         </View>
         
         <FlatList
         data={Feed.comments}
         renderItem={({item})=>{
             return(
                <View style={styles.usersView}>
                <Image source={item.avatar} style={styles.iconsStyle}/> 
                 <View>
                    <Text style={styles.tagsStyle}>{item.name}</Text>
                    <Text style={styles.nameStyle}>{item.tag}</Text>
                </View>   
                </View>   
             )
         }}
         />
            <View style={styles.commentView}>
                <View style={styles.userStyle}>
                <Text>JY</Text>
                </View>
                 <TextInput
                 placeholder='Add a comment...'
                 style={styles.inputStyle}
                 />
            </View>   

     </View>
     
  )
    
};

const styles=StyleSheet.create({
    container:{
     width:width,
     backgroundColor:Colors.white,
     marginTop:6,
    },
    userView:{
      flexDirection:'row',
      paddingVertical:10,
      alignItems:'center'
    },
    iconStyle:{
        width:40,
        height:40,
        borderWidth:0.8,
        borderColor:Colors.border ,
        borderRadius:20,
        marginHorizontal:10
    },
    imageStyle:{
      width:width,
      height:height/2
    },
    nameStyle:{
        fontSize:12,
        fontFamily:Constants.fontFamily.light
    },
    tagStyle:{
        fontSize:14,
        fontFamily:Constants.fontFamily.light,
        margin:15
    },
    likeView:{
        flexDirection:'row',
        justifyContent:'space-between',
    },
    likesIcon:{
        width:20,
        height:20,
        marginLeft:10
    },
    likeIcon:{
        width:26,
        height:22,
        marginHorizontal:10
    },
    tagIcon:{
        width:18,
        height:24,
        marginHorizontal:10
    },
    tagsStyle:{
        fontSize:14,
        fontFamily:Constants.fontFamily.light,
        color:Colors.black
    },
    iconsStyle:{
        width:32,
        height:32,
        borderWidth:0.8,
        borderColor:Colors.black ,
        borderRadius:20,
        marginHorizontal:10
    },
    usersView:{
        flexDirection:'row',
        marginLeft:10,
        paddingVertical:6,
        alignItems:'center'
      },
      inputStyle:{
          borderWidth:0.8,
          borderColor:Colors.black,
          borderRadius:20,
          marginLeft:6,
          height:40,
          width:'80%'
      },
      userStyle:{
        width:32,
        height:32,
        borderWidth:0.8,
        borderColor:Colors.black ,
        borderRadius:20,
        marginLeft:10,
        alignItems:'center',
        justifyContent:'center',
        backgroundColor:Colors.border
    },
    commentView:{
            flexDirection:'row',
            marginLeft:10,
            paddingVertical:6,
            alignItems:'center',
            width:'100%',
            marginBottom:10
    }
})

export default NewsFeed;
