import React from 'react';
import { View, Text, StyleSheet,Image,TouchableOpacity,StatusBar} from 'react-native';
import Colors from '@Config/Colors';
import Constants from '@Config/Constants';
import Images from '@Config/Images';
import LinearGradient from 'react-native-linear-gradient';
import {Icon} from 'native-base';

const Header=({backIcon,title,goBackTo,isUserAtLeft,goToProfile,
  isMsgBellIconAtRight,singleIconAtRight,titleAtCenter,goToFeeds})=>{
  
  if (isUserAtLeft && isMsgBellIconAtRight) {
    return(
      <LinearGradient 
      colors={[Colors.main_first_lg,Colors.main_second_lg]} 
      start={{x:0,y:0}}
      end={{x:1,y:0}}
      style={{paddingTop: StatusBar.currentHeight,paddingHorizontal:18}}
      >
         <StatusBar  translucent={true} backgroundColor={'transparent'}  />
        <View style={styles.headerHomeView}> 
            <TouchableOpacity 
            onPress={goToProfile}
            style={styles.nameView}>
              <Text style={styles.nametag}>YJ</Text>
            </TouchableOpacity>
            <Text style={styles.titlestyle}>{title}</Text>
            <View style={{flexDirection:'row'}}>
            <TouchableOpacity onPress={goToFeeds}><Image source={Images.Msg} style={styles.msgIcon}/></TouchableOpacity>
            <Image source={Images.Bell} style={styles.bellIcon}/>
            </View>
        </View>
      </LinearGradient>
    )
  }
  if (backIcon && isMsgBellIconAtRight) {
    return(
      <LinearGradient 

      colors={[Colors.main_first_lg,Colors.main_second_lg]} 
      start={{x:0,y:0}}
      end={{x:1,y:0}}
      style={{paddingTop: StatusBar.currentHeight,paddingHorizontal:18}}
      >
         <StatusBar  translucent={true} backgroundColor={'transparent'}  />
        <View style={styles.headerHomeView}> 
        <Icon 
          name={backIcon} 
          style={styles.iconstyle}
          onPress={goBackTo}
          />
            <Text style={styles.titlestyle}>{title}</Text>
            <View style={{flexDirection:'row'}}>
            <Image source={Images.Msg} style={styles.msgIcon}  />
            <Image source={Images.Bell} style={styles.bellIcon} />
            </View>
            
        </View>
      </LinearGradient>
    )}
  if(backIcon&&singleIconAtRight){
    return(
      <LinearGradient 
      colors={[Colors.main_first_lg,Colors.main_second_lg]} 
      start={{x:0,y:0}}
      end={{x:1,y:0}}
      style={{paddingTop: StatusBar.currentHeight,paddingHorizontal:18}}
      >
         <StatusBar  translucent={true} backgroundColor={'transparent'}  />
        <View style={styles.headerView}> 
            <Icon 
            name={backIcon} 
            style={styles.iconstyle}
            onPress={goBackTo}/>
            <Text style={styles.titlestyle}>{title}</Text>
           <Icon name={singleIconAtRight} style={{color:Colors.white,fontSize:36}}/>
        </View>
      </LinearGradient>
    )
  }

  return(
    <LinearGradient 
    colors={[Colors.main_first_lg,Colors.main_second_lg]} 
    start={{x:0,y:0}}
    end={{x:1,y:0}}
    style={{paddingTop: StatusBar.currentHeight,paddingHorizontal:18}}
    >
       <StatusBar  translucent={true} backgroundColor={'transparent'}  />
      <View style={styles.titleView}> 
          <Icon 
          name={backIcon} 
          style={styles.iconstyle}
          onPress={goBackTo}/>
          {titleAtCenter?<View style={{width:'40%'}}></View>:null}
          <Text style={styles.titlestyle}>{title}</Text>
      </View>
    </LinearGradient>
  )}
const styles=StyleSheet.create({
    headerView:{
     flexDirection:'row',
     height:55,
     alignItems:'center',
     justifyContent:'space-between'
    },
    titleView:{
      flexDirection:'row',
      height:55,
      alignItems:'center',
     },
    headerHomeView:{
      flexDirection:'row',
      height:55,
      justifyContent:'space-between',
      alignItems:'center'
     },
    iconstyle:{
      color:Colors.white,
      fontSize:26
    },
    titlestyle:{
      fontSize:20,
      color:Colors.white,
      marginLeft:15,
      
    },
    nameView:{
      width:40,
      height:40,
      borderRadius:20,
      justifyContent:'center',
      alignItems:'center',
      backgroundColor:Colors.silver
    },
    nametag:{
    color:Colors.black,
    fontFamily:Constants.fontFamily.bold
    },
    groupIcon:{
      width:36,
      height:36,
    },
    msgIcon:{
      width:26,
      height:21,
    },
    bellIcon:{
      width:20,
      height:24,
      marginLeft:25
    },
   
})

export default Header