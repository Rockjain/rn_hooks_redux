import React from 'react'
import { View,TouchableOpacity,Text,Image,StyleSheet,StatusBar} from 'react-native';
import  Colors  from '@Config/Colors';
import LinearGradient from 'react-native-linear-gradient';
import {Icon} from 'native-base';
import Constants from '@Config/Constants';
import pickImages from '@Components/pickImages';
import Languages from '@Config/Languages';

const Profile=(props)=>{
        const [pickImage,photoUri,photo]=pickImages()
       
         return(
      
            <LinearGradient 
                colors={[Colors.main_first_lg,Colors.main_second_lg]} 
                start={{x:0,y:0}}
                end={{x:1,y:0}}
                style={{padding:15,paddingTop:StatusBar.currentHeight}}>
                    <Icon name='ios-arrow-back' 
                      style={{color:Colors.white,fontSize:24}}
                      onPress={props.goBack}/>
                    <View style={styles.container}>
                    <View style={{flexDirection:'row'}}>
                    <View style={styles.profile}>
                    <TouchableOpacity>
                      {!photo ?<Text style={styles.profileText}>JY</Text>:
                      <Image source={photo} style={styles.photoStyle}/>}
                    </TouchableOpacity>
                    </View>
                      <Icon onPress={pickImage}
                      name ='camera'
                      style={{color:Colors.white,fontSize:24,marginLeft:4,marginTop:-10}}/>
                    </View>        
                      <Text style={styles.name}>Jayson Yi </Text>
                      <Text style={styles.text}>OPlc Class  |  Entering</Text>
                      <Text style={styles.textSecond}>{Languages.teacherRina}</Text>
                    </View>
            </LinearGradient>                          
      );
}
const styles=StyleSheet.create({
        container:{
            alignItems:'center',
        },
        profile:{
            width:70,
            height:70,
            marginLeft:10,
            borderRadius:35,
            backgroundColor:Colors.silver,
            justifyContent:'center',
            alignItems:'center',
        },
        profileText:{
            fontSize:16,
            fontFamily:Constants.fontFamily.bold
        },
        name:{
            fontSize:16,
            fontFamily:Constants.fontFamily.semibold,
            marginTop:5,
            color: Colors.white,
        },
        photoStyle:{
            height:70,
            width:70,
            borderRadius:35,
            
        },
        text:{
            color:Colors.white ,
            marginTop:5,
        },
        textSecond:{
              color: Colors.white,
              marginTop:10,
              marginBottom:30
        },
       
               
})
export default Profile
