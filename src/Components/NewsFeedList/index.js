import React from 'react';
import {FlatList} from 'react-native';
import NewsFeed from '@Components/NewsFeed';

const NewsFeedList = ({Feeds}) => {
  if (!Feeds) {
    return null;
  }

  return <FlatList 
         data={Feeds} 
         renderItem={({item}) => {
          return <NewsFeed Feed={item}/>
          }} />;
};

export default NewsFeedList;
