import React from 'react';
import { View, Text, StyleSheet,Image,TouchableOpacity} from 'react-native';
import Colors from '@Config/Colors';
import LinearGradient from 'react-native-linear-gradient';

const GredientView=({height})=>{
  return(
    <LinearGradient 
    colors={[Colors.main_first_lg,Colors.main_second_lg]} 
    start={{x:0,y:0}}
    end={{x:1,y:0}}
    style={{width:'100%',height:height}}>
    </LinearGradient>
  )}
export default GredientView