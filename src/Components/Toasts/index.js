import React from 'react';
import { Toast } from 'native-base';

class Toasts{

 static show=function (message) {
    Toast.show({
        text: message,
        buttonText: 'Okay',
        duration:2500,
        position:'top'
      })
 } 

}

export default Toasts