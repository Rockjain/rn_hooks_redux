import React from 'react';
import { TextInput, StyleSheet} from 'react-native';
import Colors from '@Config/Colors';
import Constants from '@Config/Constants';

const Input=({placeholder,onChange,bordercolor,fcolor})=>{
  return(
      <TextInput
        placeholder={placeholder}
        autoCapitalize='none'
        autoCorrect={false}
        placeholderTextColor={fcolor}
        onChangeText={text=>onChange(text)}
        style={[styles.inputStyle,{borderBottomColor:bordercolor,color:fcolor}]}
      />
  )
}

const styles=StyleSheet.create({
    inputStyle:{
        borderBottomWidth:1,
        height:45,
        fontSize:16,
        marginTop:25,
        fontFamily:Constants.fontFamily.medium
    }
})

export default Input