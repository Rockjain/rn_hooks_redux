import React,{useState,useEffect} from 'react';
import Languages from '@Config/Languages';
import Toasts from '@Components/Toasts';
import { useDispatch,useSelector } from 'react-redux';
import { checkUser,changePassword } from '@Api/User'; 
import { getData } from '../../../Utils/Storage';
import Constants from '../../../Config/Constants';

export default ()=>{
   
     let dispatch=useDispatch()
      let isFetching=useSelector(state=>state.User.isFetching)
      let FindUser=useSelector(state=>state.User.FindUser)
      const [name,setName]=useState('')
      const [email,setEmail]=useState('')
      const [visible,setVisible]=useState(false)
      const [phone,setPhone]=useState('')
      const [code,setCode]=useState('')
      const [password,setPassword]=useState('')
      const [repassword,setRepassword]=useState('')

    const findUser=()=>{
        if (email==''||name ==''||phone==''||code=='') {
            Toasts.show(Languages.allfieldrequired)
        } else {
            dispatch(checkUser({email,name,phone,code}))
        }
      }

      const doChange=async()=>{
        if (password==''||repassword =='') {
            Toasts.show(Languages.allfieldrequired)
        } else {
            if (password != repassword) {
                Toasts.show(Languages.passwordmustbesame)
            } else {
                let key=await getData(Constants.User_Key)
                dispatch(changePassword({phone,key,password}))
            }
        }
      }

      useEffect(()=>{
       if (FindUser) {
           setVisible(true)
       }
      },[FindUser])

        return{
            setName,
            setEmail,
            setPassword,
            setRepassword,
            setPhone,
            setCode,
            setVisible,
            visible,
            isFetching,
            doChange,
            findUser
      }
   
}