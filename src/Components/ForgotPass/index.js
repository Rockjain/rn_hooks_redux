import React from 'react';
import { View, Text,TouchableOpacity,StyleSheet,Dimensions} from 'react-native';
import Input from '@Components/Input';
import Colors from '@Config/Colors';
import Languages from '@Config/Languages';
import LinearGradient from 'react-native-linear-gradient';
const  {width,height}=Dimensions.get('window')
import Loader from '@Components/Loader';
import Dialog from 'react-native-popup-dialog';
import forgotPass from './hooks/forgotPass';

const ForgotPass=({})=>{
      
  const { setName,setEmail,setPassword,setRepassword,setPhone,setVisible,
    setCode,visible,isFetching,doChange,findUser}=forgotPass()

        return(
            <View style={{marginHorizontal:20}}>     

                <Input
                placeholder={Languages.email}
                bordercolor={Colors.line}
                fcolor={Colors.black}
                onChange={text=>setEmail(text)}
                /> 
                  <Input
                placeholder={Languages.name}
                bordercolor={Colors.line}
                fcolor={Colors.black}
                onChange={text=>setName(text)}
                /> 
                 <Input
               placeholder={Languages.phonenumber}
                bordercolor={Colors.line}
                fcolor={Colors.black}
                onChange={text=>setPhone(text)}
                />                 
                <View style={styles.certificationView}>
                <Input
              placeholder={Languages.certificationnumber} 
                bordercolor={Colors.line}
                fcolor={Colors.black}
                onChange={text=>setCode(text)}
                /> 
              <Text style={styles.verificationtext}>{Languages.sendverificationnumber}</Text>
                </View>

                <TouchableOpacity onPress={findUser}>
                <LinearGradient 
                colors={[Colors.main_first_lg,Colors.main_second_lg]} 
                start={{x:0,y:0}}
                end={{x:1,y:0}}
                style={styles.loginbotton}
                > 
                     {isFetching 
                    ?<Loader isSmall={true} color={Colors.white}/>
                    :<Text style={styles.logintext}>{Languages.confirm}</Text>
                    }
                </LinearGradient>
                </TouchableOpacity>
                <Dialog
                    visible={visible}
                    dialogStyle={{width:'80%'}}
                    onTouchOutside={() => {
                      setVisible(false);
                    }}>

                    <View style={{backgroundColor:'rgba(0,0,0,0.4)'}}>
                    <Text 
                    onPress={()=>setVisible(false)}
                    style={{fontSize:30,color:Colors.white,alignSelf:'flex-end',marginRight:10}}>x</Text>
                       <View style={{padding:10,alignItems:'center',backgroundColor:Colors.lightgrey}}>
                         <Text 
                         style={[styles.buttontext,{color:Colors.black}]}>{Languages.changingyourpassword}</Text>
                       </View>
                       <View style={{paddingHorizontal:20,paddingBottom:20,backgroundColor:Colors.white}}>                         
                          <Input
                            placeholder={Languages.enterpassword}
                            bordercolor={Colors.line}
                            fcolor={Colors.black}
                            onChange={text=>setPassword(text)}
                            /> 
                              <Input
                            placeholder={Languages.retypepassword}
                            bordercolor={Colors.line}
                            fcolor={Colors.black}
                            onChange={text=>setRepassword(text)}
                            /> 
                        </View>
                        <View  style={styles.changeView}>
                          <Text 
                          onPress={()=>{
                            setVisible(false)
                            setTimeout(() => {
                              doChange()
                            }, 50);
                          }}
                          style={{color:Colors.line}}>{Languages.change}</Text>                      
                         </View>
                       </View>
                    </Dialog>
                  </View>
        )   
}

const styles= StyleSheet.create({
   
    loginbotton:{
      width:width-100,
      height:50,
      borderRadius:25,
      justifyContent:'center',
      alignSelf:'center',
      marginTop:40
    },
    logintext:{
        fontSize:16,
        color:Colors.white,
        alignSelf:'center'
    },
    certificationView:{
        flexDirection:'row',
        justifyContent:'space-around',
    },
    verificationtext:{
        fontSize:16,
        color:Colors.line,
        alignSelf:'center',
        marginTop:25
    },
    changeView:{
      alignItems:'center',
      justifyContent:'center',
      height:50,
      borderTopWidth:1,
      borderTopColor:Colors.lightgrey,
      backgroundColor:Colors.white
    }
    
})

export default ForgotPass