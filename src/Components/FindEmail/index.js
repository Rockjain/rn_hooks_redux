import React,{useState,useEffect} from 'react';
import { View, Text,TouchableOpacity,StyleSheet,Dimensions} from 'react-native';
import Input from '@Components/Input';
import Colors from '@Config/Colors';
import LinearGradient from 'react-native-linear-gradient';
import Languages from '@Config/Languages';
import Constants from '@Config/Constants';
import Toasts from '@Components/Toasts';
import { useDispatch,useSelector } from 'react-redux';
import { checkEmail } from '@Api/User'; 
const  {width,height}=Dimensions.get('window')
import Loader from '@Components/Loader';
import Dialog, { DialogContent } from 'react-native-popup-dialog';

const FindEmail=({showForgot,navigateTo})=>{
      let dispatch=useDispatch()
      let isFetching=useSelector(state=>state.User.isFetching)
      let Email=useSelector(state=>state.User.Email)
      const [name,setName]=useState('')
      const [visible,setVisible]=useState(false)
      const [phone,setPhone]=useState('')
      const [code,setCode]=useState('')

     findEmail=()=>{
         if (name ==''||phone==''||code=='') {
             Toasts.show(Languages.allfieldrequired)
         } else {
             dispatch(checkEmail({name:name,phone:phone,code:code}))
         }
       }
       useEffect(()=>{
        if (Email) {
            setVisible(true)
        }
       },[Email])

        return(
            <View style={{marginHorizontal:20}}>     

                <Input
                placeholder={Languages.name}
                bordercolor={Colors.line}
                fcolor={Colors.black}
                onChange={text=>setName(text)}
                /> 
                 <Input
               placeholder={Languages.phonenumber}
                bordercolor={Colors.line}
                fcolor={Colors.black}
                onChange={text=>setPhone(text)}
                /> 
                
                <View style={styles.certificationView}>

                <Input
                placeholder={Languages.certificationnumber} 
                bordercolor={Colors.line}
                fcolor={Colors.black}
                onChange={text=>setCode(text)}
                /> 
                <Text style={styles.verificationtext}>{Languages.sendverificationnumber}</Text>
                </View>
                
                <TouchableOpacity onPress={findEmail}>
                <LinearGradient 
                colors={[Colors.main_first_lg,Colors.main_second_lg]} 
                start={{x:0,y:0}}
                end={{x:1,y:0}}
                style={styles.loginbotton}
                > 
                 {isFetching 
                 ?<Loader isSmall={true} color={Colors.white}/>
                :<Text style={styles.logintext}>{Languages.confirm}</Text>
                 }
                </LinearGradient>
                </TouchableOpacity>

                   <Dialog
                    visible={visible}
                    dialogStyle={{width:'80%'}}
                    onTouchOutside={() => {
                      setVisible(false);
                    }}>

                    <View style={{backgroundColor:'rgba(0,0,0,0.4)'}}>
                    <Text 
                    onPress={()=>setVisible(false)}
                    style={{fontSize:30,color:Colors.white,alignSelf:'flex-end',marginRight:10}}>x</Text>
                       <View style={{padding:10,alignItems:'center',backgroundColor:Colors.lightgrey}}>
                         <Text 
                         style={[styles.buttontext,{color:Colors.black}]}>{Languages.emailfindresult}</Text>
                       </View>
                       <View  style={{padding:10,alignItems:'center',justifyContent:'center',height:120,backgroundColor:Colors.white}}>
                         <Text style={{textAlign:'center',color:'grey'}}>{Languages.youremail} {`\n`} <Text style={{textAlign:'center',color:Colors.black}}>{Email}</Text></Text>
                       </View>
                       <View  style={{flexDirection:'row',justifyContent:'space-between',height:50,borderTopWidth:1,borderTopColor:Colors.lightgrey,backgroundColor:Colors.white}}>
                        <View style={{borderRightWidth:1,borderRightColor:Colors.lightgrey,justifyContent:'center',alignItems:'center',width:'49%'}}>
                          <Text 
                           onPress={()=>{
                            setVisible(false)
                            setTimeout(() => {
                              navigateTo()
                            }, 50);
                           
                          }}
                          style={styles.buttontext}>{Languages.signin}</Text>
                        </View>
                        <View style={{justifyContent:'center',alignItems:'center',width:'50%'}}>
                          <Text 
                          onPress={()=>{
                            setVisible(false)
                            setTimeout(() => {
                              showForgot()
                            }, 50);
                           
                          }}
                          style={styles.buttontext}>{Languages.fogotpassword}</Text>
                        </View>
                       </View>
                    </View>
                  
                    </Dialog>
            </View>
        )   
}

const styles= StyleSheet.create({
   
    loginbotton:{
      width:width-100,
      height:50,
      borderRadius:25,
      justifyContent:'center',
      alignSelf:'center',
      marginTop:40
    },
    logintext:{
        fontSize:16,
        color:Colors.white,
        alignSelf:'center'
    },
    certificationView:{
        flexDirection:'row',
        justifyContent:'space-around',
    },
    verificationtext:{
        fontSize:16,
        color:Colors.line,
        alignSelf:'center',
        marginTop:25
    },
    buttontext:{
        fontSize:16,
        fontFamily:Constants.fontFamily.bold,
        color:Colors.line,
        textAlign:'center'
    }
    
})

export default FindEmail