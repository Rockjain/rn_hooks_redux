import React from 'react';
import { View, StyleSheet, ActivityIndicator} from 'react-native';
import Colors from '@Config/Colors';

const Loader=({isSmall,color})=>{

    if (isSmall) {
        return <ActivityIndicator color={color}/>
    }
  return(
    
      <View style={styles.mainView}> 
         <ActivityIndicator color={Colors.line}/>
      </View>
   
  )
}

const styles=StyleSheet.create({
    mainView:{
    flex:1,
    justifyContent:'center',
    alignItems:'center',
    position:'absolute',
    top:0,
    left:0,
    right:0,
    bottom:0,
    zIndex:999,
    backgroundColor:'transparent'
    },
})

export default Loader