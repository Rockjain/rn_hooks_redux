import { NavigationActions, StackActions} from 'react-navigation';

let _navigator;

function setTopLevelNavigator(navigatorRef) {
  _navigator = navigatorRef;
}

function navigate(routeName, params) {
  _navigator.dispatch(
    NavigationActions.navigate({
      routeName,
      params,
    })
  );
}

function goBack(routeName, params) {
    _navigator.dispatch(
      NavigationActions.back({
        routeName,
        params,
      })
    );
  }

 function reset(routeName,params) {
    const resetAction = StackActions.reset({
        index: 0,
        actions: [NavigationActions.navigate({ routeName,params })],
      });
     _navigator.dispatch(resetAction);
 } 

 function push(routeName,params) {
    const pushAction = StackActions.push({
        routeName,
        params
      });
     _navigator.dispatch(pushAction);
 } 

// add other navigation functions that you need and export them

export default {
  setTopLevelNavigator,
  navigate,
  goBack,
  reset,
  push
};