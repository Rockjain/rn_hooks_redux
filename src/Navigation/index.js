import { createAppContainer, createSwitchNavigator} from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import { createBottomTabNavigator } from 'react-navigation-tabs';
import Colors from '@Config/Colors';
import BottomTab from '@Components/BottomTab';

import SplashScreen from '@AuthScreens/Splash';
import LoginScreen from '@AuthScreens/Login';
import ForgotScreen from '@AuthScreens/Forgot';
import RegisterScreen from '@AuthScreens/Register';


import HomeScreen from '@MainScreens/Home';
import ProfileScreen from '@MainScreens/Profile';
import FeedNewsScreen from '@MainScreens/FeedNews';
import ScheduleSlots from '@MainScreens/ScheduleSlots';
import Schedule from '@MainScreens/Schedule';
import ProfileHistory from '@MainScreens/ProfileHistory';
import MyClass from '@MainScreens/MyClass';
import FeedbackList from '@MainScreens/FeedbackList';
import Call from '@MainScreens/Call';
import OnClass1 from '@MainScreens/OnClass1';
import ChatScreen from '@MainScreens/ChatScreen';

const auth=createStackNavigator({
    login: LoginScreen,
    forgot:ForgotScreen,
    register:RegisterScreen
},
{
    initialRouteName:'login',
    headerMode:'none'
})

const timelineStack =createStackNavigator({
    home: HomeScreen,
    profile:ProfileScreen,
    feeds:FeedNewsScreen
},
{
    initialRouteName:'home',
    headerMode:'none'
})

const classStack =createStackNavigator({
    myClass:MyClass,
    fbList:FeedbackList,
},
{
    initialRouteName:'myClass',
    headerMode:'none'
})

const callStack =createStackNavigator({
    call:Call,
    onClass1:OnClass1,
    chat:ChatScreen,
},
{
    initialRouteName:'call',
    headerMode:'none'
})

const scheduleStack =createStackNavigator({
    schedule:Schedule,
    ScheduleSlots:ScheduleSlots,
},
{
    initialRouteName:'schedule',
    headerMode:'none'
})

const myStack =createStackNavigator({
    profile:ProfileScreen,
    history:ProfileHistory
},
{
    initialRouteName:'profile',
    headerMode:'none'
})

const bottomtab =createBottomTabNavigator({
    Timeline: timelineStack,
    Class:classStack,
    Schedule:scheduleStack,
    Mypage:myStack,
},
{
    tabBarComponent:BottomTab,
})


const App=createSwitchNavigator({
    splash: SplashScreen,
    Auth:auth,
    Call:callStack,
    Main:bottomtab
},{
    initialRouteName:'splash'
})


export default createAppContainer(App)