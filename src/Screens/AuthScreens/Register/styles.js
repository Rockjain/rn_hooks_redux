import { StyleSheet, Dimensions } from 'react-native';
import Colors from '@Config/Colors';
import Constants from '@Config/Constants';
const {width, height}=Dimensions.get('window')

export default StyleSheet.create({
    container:{
      flex:1 ,
      paddingHorizontal:20 
    },
    icon:{
     width:175,
     height:175,
     alignSelf:'center',
     marginTop:50
    },
    name:{
      fontSize:36,
      fontFamily:Constants.fontFamily.bold,
      color:Colors.white,
      alignSelf:'center'
    },
    tag:{
      fontSize:16,
      fontFamily:Constants.fontFamily.medium,
      color:Colors.white,
      alignSelf:'center',
      marginBottom:10
    },
   
    buttntext:{
      fontSize:16,
      fontFamily:Constants.fontFamily.medium,
      color:Colors.black,
      alignSelf:'center'
    },
    regbotton:{
      backgroundColor:Colors.white,
      width:width-100,
      height:50,
      borderRadius:25,
      justifyContent:'center',
      alignSelf:'center',
      marginVertical:40
    },
})