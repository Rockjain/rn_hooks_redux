import React from 'react';
import { View, Text, ImageBackground,TouchableOpacity,ScrollView ,Image,StatusBar} from 'react-native';
import styles from './styles';
import Images from '@Config/Images';
import Languages from '@Config/Languages';
import Input from '@Components/Input';
import Colors from '@Config/Colors';
import Toasts from '@Components/Toasts';
import { connect } from 'react-redux';
import { registerUser } from '@Api/User';
import Loader from '@Components/Loader';

class Register extends React.Component{
    constructor(props){
         super(props)
         this.state={
             korean_name:'',
             english_name:'',
             castle:'',
             phone_number:'',
             email:'',
             password:'',
             re_password:''
         }
    }

    doRegister=async()=>{
       const {korean_name,english_name,castle,phone_number,email,password,re_password }=this.state
       if (korean_name == '' || english_name == '' || castle == '' || phone_number == '' || 
       email == '' || password == '' || re_password == '') {
           Toasts.show('All fields required!')
       }else if(password != re_password){
        Toasts.show('Password an repassword must be same!')
       }else{
           this.props.registerUser(email,password,korean_name,english_name,castle,phone_number)

       }
    }

    render(){
        const {navigation,isFetching}=this.props
        return(
            
            <ImageBackground 
            source={Images.Splash_bg}
            style={styles.container}
            >
                <ScrollView 
                showsVerticalScrollIndicator={false}
                >
               <Image source={Images.Group} style={styles.icon}/>     
              <Text style={styles.name}>{Languages.appname}</Text>
              <Text style={styles.tag}>{Languages.speakinenglish}</Text>

                <Input
                placeholder={Languages.koreanname}
                bordercolor={Colors.white}
                fcolor={Colors.white}
                onChange={text=> this.setState({korean_name:text})}
                /> 

                <View style={{flexDirection:'row',justifyContent:'space-between'}}>
                  <View style={{width:'55%'}}>
                    <Input
                    placeholder={Languages.englishname}
                    bordercolor={Colors.white}
                    fcolor={Colors.white}
                    onChange={text=> this.setState({english_name:text})}
                    /> 
                     </View>
                  <View style={{width:'40%'}}>
                    <Input
                    placeholder={Languages.castle}
                    bordercolor={Colors.white}
                    fcolor={Colors.white}
                    onChange={text=> this.setState({castle:text})}
                    /> 
                     </View>
                </View>
               
                 <Input
                placeholder={Languages.phonenumber}
                bordercolor={Colors.white}
                fcolor={Colors.white}
                onChange={text=> this.setState({phone_number:text})}
                /> 
                
                 <Input
                 placeholder={Languages.email}
                bordercolor={Colors.white}
                fcolor={Colors.white}
                onChange={text=> this.setState({email:text})}
                /> 

               <Input
                 placeholder={Languages.enterpassword}
                bordercolor={Colors.white}
                fcolor={Colors.white}
                onChange={text=> this.setState({password:text})}
                /> 
                 <Input
                placeholder={Languages.retypepassword}
                bordercolor={Colors.white}
                fcolor={Colors.white}
                onChange={text=> this.setState({re_password:text})}
                /> 
            
                <TouchableOpacity 
                onPress={this.doRegister}
                style={styles.regbotton}>
                   {isFetching 
                   ? <Loader isSmall={true} color={Colors.line}/> 
                   : <Text style={styles.buttntext}>{Languages.signup}</Text>
                   }

                </TouchableOpacity>
                </ScrollView>
                <StatusBar  translucent={true} backgroundColor={'transparent'}  />
            </ImageBackground>
           
        )
    }
}

const mapStateToProps=(state)=>{
    return{
        isFetching:state.User.isFetching,
        User:state.User.User,
        Error:state.User.Error
    }
   }
   
   export default connect(mapStateToProps,{registerUser})(Register)