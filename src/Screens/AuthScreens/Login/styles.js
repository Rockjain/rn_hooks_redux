import { StyleSheet, Dimensions } from 'react-native';
import Colors from '@Config/Colors';
import Constants from '@Config/Constants';
const {width, height}=Dimensions.get('window')

export default StyleSheet.create({
    container:{
      flex:1 ,
      paddingHorizontal:20 
    },
    icon:{
     width:175,
     height:175,
     marginTop:50,
     alignSelf:'center'
    },
    name:{
      fontSize:36,
      fontFamily:Constants.fontFamily.bold,
      color:Colors.white,
      alignSelf:'center'
    },
    tag:{
      fontSize:16,
      fontFamily:Constants.medium,
      color:Colors.white,
      alignSelf:'center',
      marginBottom:10
    },
    findText:{
      fontSize:16,
      fontFamily:Constants.fontFamily.medium,
      color:Colors.white,
      alignSelf:'flex-end',
      marginTop:20
    },
    loginbotton:{
      backgroundColor:Colors.white,
      width:width/2,
      height:50,
      borderRadius:25,
      justifyContent:'center',
      alignSelf:'center',
      marginTop:25
    },
    logintext:{
      fontSize:16,
      fontFamily:Constants.fontFamily.medium,
      color:Colors.black,
      alignSelf:'center'
    },
    regbotton:{
      borderWidth:1,
      borderColor:Colors.white,
      width:width-100,
      height:50,
      borderRadius:25,
      justifyContent:'center',
      alignSelf:'center',
      marginTop:40
    },
})