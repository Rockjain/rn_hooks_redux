import React from 'react';
import { View, Text, ImageBackground,TouchableOpacity,Image,StatusBar} from 'react-native';
import styles from './styles';
import Images from '@Config/Images';
import Input from '@Components/Input';
import Loader from '@Components/Loader';
import Colors from '@Config/Colors';
import Languages from '@Config/Languages';
import Toasts from '@Components/Toasts';
import { connect } from 'react-redux';
import { validateUser } from '@Api/User';

class Login extends React.Component{
    constructor(props){
        super(props)
        this.state={
            email:'',
            password:'',
        }
   }

    UNSAFE_componentWillReceiveProps=(nextProps)=>{
        if (nextProps.User) {
            console.log(nextProps.User)
        }
    }
    doLogin=()=>{
     const {email,password}=this.state
     if (email == '' || password == '') {
         Toasts.show('Please enter email & password!')
     } else {
        this.props.validateUser(email,password)
     }
    }
    render(){
      
        const {navigation,isFetching,User}=this.props
        return(
            <ImageBackground 
            source={Images.Splash_bg}
            style={styles.container}
            >
                <Image source={Images.Group} style={styles.icon}/>
              <Text style={styles.name}>{Languages.appname}</Text>
              <Text style={styles.tag}>{Languages.speakinenglish}</Text>
                <Input
                placeholder={Languages.email}
                bordercolor={Colors.white}
                fcolor={Colors.white}
                onChange={text=> this.setState({email:text})}
                /> 
                 <Input
                placeholder={Languages.password}
                bordercolor={Colors.white}
                fcolor={Colors.white}
                onChange={text=> this.setState({password:text})}
                />
                <Text 
                onPress={()=> navigation.push('forgot')}
                style={styles.findText}
                >{Languages.findemailpass}</Text>

                <TouchableOpacity 
                onPress={this.doLogin}
                style={styles.loginbotton}>
                    {isFetching 
                   ? <Loader isSmall={true} color={Colors.line}/> 
                    :<Text style={styles.logintext}>{Languages.login}</Text>
                    }
                </TouchableOpacity>

                <TouchableOpacity 
                    onPress={()=> navigation.push('register')}
                    style={styles.regbotton}>
                    <Text style={styles.tag}>{Languages.notstudentyet}</Text>
                </TouchableOpacity>
                <StatusBar  translucent={true} backgroundColor={'transparent'}  />
            </ImageBackground>
        )
    }
}

const mapStateToProps=(state)=>{
 return{
     isFetching:state.User.isFetching,
     User:state.User.User,
     Error:state.User.Error
 }
}

export default connect(mapStateToProps,{validateUser})(Login)