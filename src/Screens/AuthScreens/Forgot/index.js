import React from 'react';
import { View, Text, ImageBackground,TouchableOpacity } from 'react-native';
import styles from './styles';
import Colors from '@Config/Colors';
import LinearGradient from 'react-native-linear-gradient';
import Header from '@Components/Header';
import FindEmail from "@Components/FindEmail";
import ForgotPass from "@Components/ForgotPass";
import Languages from '@Config/Languages';
export default class Forgot extends React.Component{
    constructor(props){
        super(props)
        this.state={
            isFindEmail:true
        }
    }

    
    render(){
        const {isFindEmail}=this.state
        const {navigation}=this.props
        return(
            <View style={styles.container}>
              <Header
              backIcon='md-arrow-back'
              title='Find Email / Password'
              goBackTo={()=>navigation.goBack()}
              />
              <LinearGradient 
                colors={[Colors.main_first_lg,Colors.main_second_lg]} 
                start={{x:0,y:0}}
                end={{x:1,y:0}}
                style={styles.linearGradient}
                > 
                 <View style={styles.tabView}>
                    <TouchableOpacity 
                    onPress={()=>this.setState({isFindEmail:true})}
                    style={[styles.tab,{backgroundColor:isFindEmail?Colors.white:Colors.transparent}]}>
                        <Text style={[styles.tabtitle,{color:isFindEmail?Colors.black:Colors.white}]}>{Languages.findemail}</Text>
                        </TouchableOpacity>
                    <TouchableOpacity 
                    onPress={()=>this.setState({isFindEmail:false})}
                    style={[styles.tab,{backgroundColor:isFindEmail?Colors.transparent:Colors.white}]}>
                         <Text style={[styles.tabtitle,{color:isFindEmail?Colors.white:Colors.black}]}>{Languages.fogotpassword}</Text>
                        </TouchableOpacity>
                </View>
                </LinearGradient>

                 <View style={{marginTop:-25,borderRadius:25,backgroundColor:Colors.white}}>
                    {isFindEmail 
                    ? <FindEmail 
                    showForgot={()=>this.setState({isFindEmail:false})}
                    navigateTo={()=>navigation.goBack()}
                    /> 
                    : <ForgotPass/>
                    }  
                 </View> 
              
            </View>
        )
    }
}