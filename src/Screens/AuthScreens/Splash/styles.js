import { StyleSheet, Dimensions} from 'react-native';
import Colors from '@Config/Colors';
import Constants from '@Config/Constants';
const {width, height}=Dimensions.get('window')

export default StyleSheet.create({
    container:{
      flex:1,
      alignItems:'center'
    },
    icon:{
      width:175,
      height:175,
      marginTop:height/4
    },
    name:{
      fontSize:36,
      fontFamily:Constants.fontFamily.bold,
      color:Colors.white,
    },
    tag:{
      fontSize:16,
      fontFamily:Constants.fontFamily.medium,
      color:Colors.white
    },
    copyrightView:{
      position:'absolute',
      left:0,
      right:0,
      bottom:0,
      height:40,
      alignItems:'center'
    },
    copyright:{
      fontSize:14,
      fontFamily:Constants.fontFamily.medium,
      color:Colors.white,
    }
})