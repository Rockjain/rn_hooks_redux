import React from 'react';
import { View, Text, ImageBackground,Image,StatusBar} from 'react-native';
import styles from './styles';
import Images from '@Config/Images';
import Languages from '@Config/Languages';
import Constants from '@Config/Constants';
import { getData } from '../../../Utils/Storage';
export default class Splash extends React.Component{

    UNSAFE_componentWillMount=async()=>{
        let userstring=await getData(Constants.User)
        let user=JSON.parse(userstring)
        console.log('user',user)
        if (user) {
            setTimeout(() => {
                this.props.navigation.navigate('Main') 
             }, 2000);
        } else {
            setTimeout(() => {
                this.props.navigation.navigate('Auth') 
             }, 2000);
        }
       
    }

    render(){
        return(
            <ImageBackground 
            source={Images.Splash_bg}
            style={styles.container}
             >
                <Image source={Images.Group} style={styles.icon}/>
                <Text style={styles.name}>{Languages.appname}</Text>
                <Text style={styles.tag}>{Languages.speakinenglish}</Text>
            
                <View style={styles.copyrightView}>
                   <Text style={styles.copyright}>{Languages.copyright}</Text>
                </View>
                <StatusBar  translucent={true} backgroundColor={'transparent'}  />
            </ImageBackground>
        )
    }
}