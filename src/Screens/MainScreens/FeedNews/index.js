import React from 'react';
import { View, Text,Image,TouchableOpacity,TextInput} from 'react-native';
import styles from './styles';
import Images from '@Config/Images';
import Header from '@Components/Header';
import Loader from '../../../Components/Loader';
import { connect } from 'react-redux';
import AsyncStorage from '@react-native-community/async-storage';
import { Icon } from 'native-base';
import ImagePicker from 'react-native-image-picker';
import  {addFeeds}  from '@Api/Feeds';
import Constants from '../../../Config/Constants';

 class FeedNews extends React.Component{
   constructor(props){
       super(props)
       this.state = {
        filepath: {
          data: '',
          uri: ''
        },
        fileData: '',
        fileUri: '',
        title:'',
        User:null
      }
    }
    UNSAFE_componentWillMount=async()=>{
            
          let userString=await AsyncStorage.getItem(Constants.User)
          let User=JSON.parse(userString)
         this.setState({User:User})
    }

    UNSAFE_componentWillReceiveProps=(nextProps)=>{
      if (nextProps.Feeds) {
          console.log(nextProps.Feeds)
      }
  }
    saveFeeds=()=>{
      const {fileData,title,User}=this.state
      if (fileData=='') {
          Toasts.show('Please select the image')
      } else {
        this.props.addFeeds({Phone_Number:User.Phone_Number,feed_Title:title,feed_Image:fileData})
      }
     }
    render(){ 
      const {navigation,isFetching,Feeds}=this.props

      const pickImage =()=>{ 

        ImagePicker.showImagePicker({title: "Pick an Image", maxWidth: 400, maxHeight: 400}, res => {
        if (res.didCancel) {
            console.log("User cancelled!");
        } else if (res.error) {
            console.log("Error", res.error);
        } else {
         
          this.setState({
            filePath: res,
            fileData: res.data,
            fileUri: res.uri
          });
        }
       
        });
}   
        return(
            <View style={styles.container}>
              <Header
              backIcon="ios-arrow-back"
              backSize={24}
              title='YCSE'
              isMsgBellIconAtRight={true}
             
              goBackTo={()=> navigation.goBack()}
              />
              {isFetching?
             <Loader />
            :null}
             <View style={styles.namegrammerView}>
               <View style={{flexDirection:'row',alignItems:'center'}}>
                  <View style={styles.nametag}>
                    <Text style={styles.nametagtext}>YJ</Text>
                  </View>
                  <Text style={styles.nametext}>Jyson Lee</Text>
                </View>
                <View style={styles.grammerView}>
                  <Image source={Images.Right} style={{width:24,height:24,margin:5}}/>
                  <Text style={styles.grammerchecktext}>Grammer Check</Text>
                </View>
             </View>
             <View style={{marginHorizontal:15}}>
               <Text >What did you have for Lunch today</Text>
               <TextInput onChangeText={text=>this.setState({title:text})} placeholder='Enter the Title'/>
             </View>
              <View>
                <Image source={{uri: 'data:image/jpeg;base64,' + this.state.fileData}} style={{width:'100%',height:350}}/>
              </View>
             <View style={styles.bottomView}>
               <View style={styles.cameraView}>
                 <Icon name='camera' style={{fontSize:40}} onPress={pickImage}/>
               </View>
               <TouchableOpacity style={styles.bulbView} onPress={this.saveFeeds}>
                <Image source={Images.Close} style={{width:36,height:36}}/>
              </TouchableOpacity>
             </View>
            </View>
        )
    }
}
const mapStateToProps=({Feed})=>{
  const {isFetching}=Feed
  return{
    isFetching
  }
 }
export default connect(mapStateToProps,{addFeeds})(FeedNews)