import React from 'react';
import { View, Text, TextInput,} from 'react-native';
import styles from './styles';
import NewsFeedList from '@Components/NewsFeedList';
import Header from '@Components/Header';
import Colors from '@Config/Colors';
import LinearGradient from 'react-native-linear-gradient';
import Menu from 'react-native-material-menu';
import Constants from '@Config/Constants';
import { Icon } from 'native-base';
import { connect } from 'react-redux';
import { getFeeds } from '@Api/Feeds';
import { getData } from '../../../Utils/Storage';
class Home extends React.Component{
 
          UNSAFE_componentWillMount=async()=>{
            let userString=await getData(Constants.User)
            let User=JSON.parse(userString)
           this.props.getFeeds({phone:User.Phone_Number});   
              
          }

          _menu = null;
                  
          setMenuRef = ref => {
            this._menu = ref;
          };
        
          hideMenu = () => {
            this._menu.hide();
          };
        
          showMenu = () => {
            this._menu.show();
          };
          
            render(){
             
            const {navigation}=this.props
              return(
                
            <View style={styles.container}>
              <Header
              title='YCSE'
              isUserAtLeft={true}
              isMsgBellIconAtRight={true}
              goToFeeds={()=>navigation.navigate('feeds')}
              goToProfile={()=>navigation.navigate('profile')}
              />
             <LinearGradient 
                colors={[Colors.main_first_lg,Colors.main_second_lg]} 
                start={{x:0,y:0}}
                end={{x:1,y:0}}
                style={{padding:18}}> 
              <Menu style={{width:'90%',borderRadius:20,padding:10}}
                      ref={this.setMenuRef}
                      button={
                        <TextInput 
                        placeholder='Tell us what do you think freely.'
                        placeholderTextColor={Colors.black}
                        style={styles.searchinput}
                        editable={true}
                        onKeyPress={this.showMenu}
                        />}>
                      <View style={{height:250,padding:15}}> 
                      <Text style={{fontFamily:Constants.fontFamily.light}}>Tell us what do you think freely</Text>
                      <View style={{height:'90%'}}></View>
                      <View style={{flexDirection:'row',justifyContent:'space-between'}}>
                        <Icon name='camera'/>
                        <Text style={{paddingRight:15,fontFamily:Constants.fontFamily.medium}}>Completed</Text>
                      </View>
                      </View>                 
                    </Menu>
                    </LinearGradient> 
                   
                  <NewsFeedList Feeds={this.props.Feeds?this.props.Feeds:[]}
                 
                     />
                    
                  </View>
        )
    }
}
const mapStateToProps=(state)=>{
  return{
      isFetching:state.Feed.isFetching,
      Feeds:state.Feed.Feeds,
      Error:state.Feed.Error
  }
 }
export default connect(mapStateToProps,{getFeeds})(Home)