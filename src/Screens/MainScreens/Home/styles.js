import { StyleSheet } from 'react-native';
import Colors from '@Config/Colors';

export default StyleSheet.create({
    container:{
      flex:1,
      backgroundColor: Colors.home_bg
    },
    searchinput:{
      borderWidth:0.8,
      borderColor:Colors.home_bg,
      borderRadius:20,
      height:40,
      alignSelf:'center',
      width:'100%',
      paddingLeft:10,
      backgroundColor:Colors.white,
      color:Colors.black
    }
})