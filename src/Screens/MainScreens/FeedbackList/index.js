import React from 'react';
import { View,Text,FlatList } from 'react-native';
import styles from './styles';
import Feedback from '@Components/Feedback';
import Header from '@Components/Header';
export default class FeedbackList extends React.Component{
    constructor(props){
        super(props)
        this.state={
            Feedback:[
                {
                    title:'feddfjd'
                },
                {
                    title:'feddfjd'
                },
                {
                    title:'feddfjd'
                },
                {
                    title:'feddfjd'
                }
            ]
        }
    }
    renderItems=({item})=>{
        return (
            <Feedback/>
        )
    }
    render(){
        const{navigation}=this.props;
        return(
            <View style={styles.container}>
                 <Header
                 backIcon='ios-arrow-back'
                 title='Feedback'
                 goBackTo={()=>navigation.goBack()}
                 isMsgBellIconAtRight={true} />
           <FlatList
           data={this.state.Feedback}
        renderItem={this.renderItems}
           />
            </View>
        )
    }
}