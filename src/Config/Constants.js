export default {

   
    User:'user',
    User_Key:'userkey',
    Language:'language',

    fontFamily:{
          bold:'apple-sd-gothic-neo-bold',
          semibold:'apple-sd-gothic-neo-semi-bold',
          regular:'apple-sd-gothic-neo-regular',
          medium:'apple-sd-gothic-neo-medium',
          light:'apple-sd-gothic-neo-light'
      }

}