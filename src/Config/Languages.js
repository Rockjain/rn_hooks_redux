import LocalizedStrings from 'react-native-localization';


let Languages = new LocalizedStrings({
 en:{

   appname:"YES",
   speakinenglish:"Your English School",
   copyright:"© 2018, YES all righ",
   findemailpass:'Find Email / Password',

   //Messages
    allfieldrequired:'All field required!',
    passwordmustbesame:'Password & retype password must be same!',

   //Login form
   email:'Email',
   password:'Password',
   login:'login',
   notstudentyet:'Not a student yet?',

   //Register form
   koreanname:'Korean name',
   englishname:'English name',
   castle:'English castle',
   phonenumber:'Phone number',
   enterpassword:'Enter Password',
   retypepassword:'Retype password',
   signup:'Sign Up',

   //Forgot password
   findemail:'Find email',
   fogotpassword:'Forgot your password',
   name:'Name',
   certificationnumber:'Certification Number',
   sendverificationnumber:'Send Verification Number',
   confirm:'Confirm',
   emailfindresult:'Email find result',
   youremail:'Your email is',
   signin:'Sign in',
   changingyourpassword:'Changing your password',
   change:'Change',

  

 
  //Feedback
  feedback:'Feedback',
  teacher:'Teacher Reena',
  june:'june 1 2018',
  great:'You had a great job today.',
  ddGreat:'You had a ddgreat job today.'
 },

 kr: {
  appname:"YES",
   speakinenglish:"당신의 영어 학교",
   copyright:"© 2018, 예 괜찮습니다",
   findemailpass:'이메일 / 비밀번호 찾기',
   
    //Messages
    allfieldrequired:'모든 필드가 필요합니다!',
    passwordmustbesame:'비밀번호와 비밀번호를 다시 입력해야합니다!',

   //Login form
   email:'이메일',
   password:'암호',
   login:'로그인',
   notstudentyet:'아직 익스 수강생이 아니신가요?',

   //Register form
   koreanname:'한국어 이름',
   englishname:'영문 이름',
   castle:'영문 성',
   phonenumber:'전화번호',
   enterpassword:'비밀번호 입력',
   retypepassword:'비밀번호 재입력',
   signup:'회원가입',

   //Forgot password
   findemail:'이메일 찾기',
   fogotpassword:'비밀번호 찾기',
   name:'이름',
   certificationnumber:'인증번호',
   sendverificationnumber:'인증번호 전송',
   confirm:'확인',
   emailfindresult:'이메일 찾기 결과',
   youremail:'회원님의 이메일은',
   signin:'로그인하기',
   changingyourpassword:'비밀번호 변경하기',
   change:'변경하기',

    

      //Call
      calling:'선생님 Reena가 전화하고 있습니다 ...',
      yes:'예',
      school:'당신의 영어 학교',

      //Feedback
      feedback:'피드백',
      teacher:'선생님 리나',
      june:'2018 년 6 월 1 일',
      great:'오늘 좋은 직장을 가졌습니다',
      ddGreat:'당신은 오늘 굉장한 직업을 가졌습니다.'
 }
});

export default Languages