export default {
    Splash_bg:require('@assets/Images/splash.png'),
    Group:require('@assets/Images/group.png'),
    Tag:require('@assets/Images/tag.png'),
    Like:require('@assets/Images/like.png'),
    Comment:require('@assets/Images/comment.png'),
    Food:require('@assets/Images/food.png'),
    Likes:require('@assets/Images/likes.png'),
    Comments:require('@assets/Images/comments.png'),
    GroupChat:require('@assets/Images/group_chat.png'),
    Bell:require('@assets/Images/bell.png'),
    Hamburger:require('@assets/Images/hamburger.png'),
    Msg:require('@assets/Images/msg.png'),
    Right:require('@assets/Images/right.png'),
    Close:require('@assets/Images/close.png'),
    Bulb:require('@assets/Images/bulb.png'),
    Bubble:require('@assets/Images/bubble1.png'),
    CloseCamera:require('@assets/Images/close_camera.png'),
    Rotate_Camera:require('@assets/Images/rotate_camera.png'),
    Book:require('@assets/Images/book.png'),
    Rina:require('@assets/Images/rina.png'),
    My:require('@assets/Images/my.png'),


    //tab icon

    Timeline:require('@assets/Images/timeline.png'),
    Class:require('@assets/Images/class.png'),
    Schedule:require('@assets/Images/schedule.png'),
    My:require('@assets/Images/my.png'),
    Color_Timeline:require('@assets/Images/color_timeline.png'),
    Color_Class:require('@assets/Images/color_class.png'),
    Color_Schedule:require('@assets/Images/color_schedule.png'),
    Color_My:require('@assets/Images/color_my.png'),
    
}