export default {
  white:'#fff',
  black:'#000',
  main_first_lg:'#71DBBE',
  main_second_lg:'#8FB9F4',
  transparent:'transparent',
  line:"#3EC6AB",
  border:'#979797',
  home_bg:'#D8D8D8',
  profile_first_lg:'#5B247A',
  profile_second_lg:'#1BCEDF',
  border_orange:'#F5A623',
  silver:'rgba(255, 255, 255, 0.6)',
  lightgrey:'#F8F8F8',
  bubbles:'#FFEFC2'
}