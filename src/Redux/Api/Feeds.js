import firebase from '@react-native-firebase/app';
import '@react-native-firebase/database';
import Toasts from '@Components/Toasts';

export const addFeeds=(data)=> {
    return async function(dispatch) {
        
   try {
     dispatch({type:'Add_Feeds_Request'})


     const ref = firebase.database().ref(`/Feeds`);
     ref.push(data).then((feeds)=>{

     console.log("feed added successfulllly")
          dispatch({type:'Add_Feeds_Success'})
          
      }).catch((error)=>{
          Toasts.show(error.message)
          dispatch({type:'Add_Feeds_Error'})  
      })
      
   } catch (error) {
        console.log(error.message)
       Toasts.show(error.message)
        dispatch({type:'Add_Feeds_Error'})     
   }
 }
}

export const getFeeds=(data)=> {
  return async function(dispatch) {
      
 try {
   dispatch({type:'Get_Feeds_Request'})


   const ref = firebase.database().ref(`/Feeds`);
   const snapshot= await ref.orderByChild('Phone_Number').equalTo(data.phone).once('value')
   if (snapshot.exists()) {
     let feedArray=[]
       snapshot.forEach(function (feed) {
           let key=feed.key
           let feeds=feed.val()
           feedArray.push(feeds)
           
       });
       dispatch({type:'Get_Feeds_Success',feeds:feedArray})

   }else{
     Toasts.show('Feed not exists!')
      dispatch({type:'Get_Feeds_Error'})
   }

 } catch (error) {
      console.log(error.message)
      Toasts.show(error.message)
      dispatch({type:'Get_Feeds_Error'})     
 }
}
}

