import firebase from '@react-native-firebase/app';
import '@react-native-firebase/database';
import '@react-native-firebase/firestore';
import '@react-native-firebase/auth';
import Toasts from '@Components/Toasts';
import NavigationService from '@Navigation/NavigationService';
import Constants from '../../Config/Constants';
import { saveData } from '../../Utils/Storage';

 export const validateUser=(email,password)=> {
     return async function(dispatch) {
         
    try {
      dispatch({type:'Validate_User_Request'})
   
    firebase.auth().signInWithEmailAndPassword(email,password)
    .then(()=>{
        firebase.firestore().collection('Users')
        .doc(firebase.auth().currentUser.uid)
        .get()
        .then((snapshot)=>{
          let user=snapshot.data()
          saveData(Constants.User,JSON.stringify(user))
          Toasts.show('Login Successfully')
          dispatch({type:'Validate_User_Success',user:user})
          NavigationService.navigate('Main')
        }).catch((error)=>{
          Toasts.show(error.message)
          dispatch({type:'Validate_User_Error'})
        })
     }).catch((error)=>{
      Toasts.show(error.message)
      dispatch({type:'Validate_User_Error'})
     })
    } catch (error) {
        console.log(error.message)
        Toasts.show(error.message)
         dispatch({type:'Validate_User_Error'})     
    }
  }
}

export const registerUser=(email,password,kname,ename,castle,phone)=> {
    return async function(dispatch) {
        
   try {
     dispatch({type:'Register_User_Request'})
     firebase.auth().createUserWithEmailAndPassword(email,password)
     .then(()=>{
       firebase.firestore().collection('Users')
       .doc(firebase.auth().currentUser.uid)
       .set({
         UId:firebase.auth().currentUser.uid,
         Castle:castle,
         Email:email,
         Phone:phone
       })
       .then(()=>{
        Toasts.show("User registered successfully")
        dispatch({type:'Register_User_Success'})
        NavigationService.push('login')
       }).catch((error)=>{
        Toasts.show(error.message)
        dispatch({type:'Register_User_Error'})     
       })
     }).catch((error)=>{
      console.log(error.message)
      Toasts.show(error.message)
      dispatch({type:'Register_User_Error'})     
     })

   } catch (error) {
        console.log(error.message)
       Toasts.show(error.message)
        dispatch({type:'Register_User_Error'})     
   }
 }
}

export const checkEmail=(action)=> {
  return async function(dispatch) {
      
 try {
   dispatch({type:'Find_Email_Request',action})
   firebase.firestore().collection('User').where()
 const ref = firebase.database().ref('/Users');
 const snapshot= await ref.orderByChild('Phone_Number').equalTo(action.phone).once('value')
    let userarray=[]
 if (snapshot.exists()) {
     snapshot.forEach(function (users) {
      userarray.push(users.val())
     });

     let user=userarray.filter(user=>{
        return user.English_Name==action.name && user.Phone_Number == action.phone
    })
    if (user.length>0) {
      console.log('Find Email : ',user[0].Email)
      Toasts.show('Email find successfully')
      dispatch({type:'Find_Email_Success',email:user[0].Email})
      
    }else{
      Toasts.show('details mismatch!')
      dispatch({type:'Find_Email_Error'})
    }    

 }else{
   Toasts.show('User not found !')
    dispatch({type:'Find_Email_Error'})
 }
 } catch (error) {
     console.log(error.message)  
     Toasts.show(error.message)
      dispatch({type:'Find_Email_Error'})     
 }
}
}
export const checkUser=(action)=> {
  return async function(dispatch) {
      
 try {
   dispatch({type:'Find_User_Request',action})
 const ref = firebase.database().ref('/Users');
 const snapshot= await ref.orderByChild('Phone_Number').equalTo(action.phone).once('value')
    let userarray=[]
 if (snapshot.exists()) {
     snapshot.forEach(function (users) {
      console.log('Find key : ',users.key)
      console.log('Find value : ',users.val())
      userarray.push({key:users.key,value:users.val()})
     });

     let user=userarray.filter(user=>{
        return user.value.English_Name==action.name && user.value.Phone_Number == action.phone && user.value.Email == action.email
    })
    if (user.length>0) {
      console.log('Find user : ',user[0])
      saveData(Constants.User_Key,user[0].key)
      dispatch({type:'Find_User_Success',user:true})
      
    }else{
      Toasts.show('User not found !')
      dispatch({type:'Find_User_Error'})
    }    

 }else{
   Toasts.show('User not found !')
    dispatch({type:'Find_User_Error'})
 }
 } catch (error) {
      console.log(error.message)
     Toasts.show(error.message)
      dispatch({type:'Find_User_Error'})     
 }
}
}

export const changePassword=(action)=> {
  return async function(dispatch) {   
      
  try {
    dispatch({type:'Change_Password_Request',action})
      const ref = firebase.database().ref('/Users/'+action.key);
      ref.update({Password:action.password})
      Toasts.show('Password change successfully!')
      dispatch({type:'Change_Password_Success'})
      NavigationService.push('login')
  } catch (error) {
      console.log(error.message)
      Toasts.show(error.message)
      dispatch({type:'Change_Password_Error'})     
  }
 } 
}