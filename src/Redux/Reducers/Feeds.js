feedsState={
    isFetching:false,  
    Feeds:[]
}

export default (state=feedsState,action)=>{
  switch (action.type) {      
      case 'Add_Feeds_Request':
        return{...state,isFetching:true }
      case 'Add_Feeds_Success':
        return{...state,isFetching:false }
      case 'Add_Feeds_Error':
        return{...state,isFetching:false}
        case 'Get_Feeds_Request':
        return{...state,isFetching:true }
      case 'Get_Feeds_Success':
        return{...state,isFetching:false,Feeds:action.feeds }
      case 'Get_Feeds_Error':
        return{...state,isFetching:false}          
      default:
        return state;
  }
}