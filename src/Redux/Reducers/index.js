import { combineReducers } from 'redux';
import User from '@Reducers/User';
import Feed from '../Reducers/Feeds';
export default combineReducers({
  User,
  Feed
})