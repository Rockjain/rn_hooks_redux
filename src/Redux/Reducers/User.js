userstate={
    isFetching:false,
    User:null,
    FindUser:false,
    Error:false,
    Email:''
}

export default (state=userstate,action)=>{
  switch (action.type) {
      case 'Validate_User_Request':
        return{...state,isFetching:true }
      case 'Validate_User_Success':
        return{...state,User:action.user,isFetching:false }
      case 'Validate_User_Error':
        return{...state,isFetching:false,Error:true }
      case 'Register_User_Request':
        return{...state,isFetching:true }
      case 'Register_User_Success':
        return{...state,User:action.user,isFetching:false }
      case 'Register_User_Error':
        return{...state,isFetching:false,Error:true }  
      case 'Find_Email_Request':
        return{...state,isFetching:true }
      case 'Find_Email_Success':
        return{...state,Email:action.email,isFetching:false }
      case 'Find_Email_Error':
        return{...state,isFetching:false,Error:true }
      case 'Find_User_Request':
        return{...state,isFetching:true }
      case 'Find_User_Success':
        return{...state,FindUser:action.user,isFetching:false }
      case 'Find_User_Error':
        return{...state,isFetching:false,Error:true }
      case 'Change_Password_Request':
        return{...state,isFetching:true }
      case 'Change_Password_Success':
        return{...state,isFetching:false }
      case 'Change_Password_Error':
        return{...state,isFetching:false,Error:true }
      default:
        return state;
  }
}