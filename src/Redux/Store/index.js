import { createStore, applyMiddleware } from 'redux'
import createSagaMiddleware from 'redux-saga'
import logger from 'redux-logger';
import reducers from '../Reducers'
import thunk from 'redux-thunk';

const sagaMiddleware=createSagaMiddleware()

const store=createStore(reducers,applyMiddleware(sagaMiddleware,thunk,logger))

export default store